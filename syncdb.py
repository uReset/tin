# coding:utf8

import ConfigParser
from peewee import MySQLDatabase

from lib.hail import db_proxy
from model.user import User
from model.light import (Light,
                         UserLight)

cfg = ConfigParser.ConfigParser()
cfg.read('config_sample.conf')


mysql_db = MySQLDatabase(database=cfg.get('DATABASE', 'database'),
                         host=cfg.get('DATABASE', 'host'),
                         port=cfg.getint('DATABASE', 'port'),
                         user=cfg.get('DATABASE', 'user'),
                         password=cfg.get('DATABASE', 'password'),)

db_proxy.initialize(mysql_db)

mysql_db.connect()
# mysql_db.create_tables([User, Light, UserLight])
User.create_table()
Light.create_table()
UserLight.create_table()

# coding:utf8

import uuid
import json

import tornado.web
import tornado.escape

from lib.session import (Session,
                         InvalidSessionException)
from model.user import User


class BaseHandler(tornado.web.RequestHandler):

    def __init__(self, *args, **kwargs):
        super(BaseHandler, self).__init__(*args, **kwargs)
        self.session = Session(request_handler=self,
                               store=self.application.redis_store)
        # self.jinja2 = self.settings.get('jinja2')
        self.json_args = {}

    def prepare(self):
        session_id = self.get_secure_cookie('sessionid', None)
        if not session_id:
            session_id = uuid.uuid1().hex
            self.set_secure_cookie('sessionid', session_id, expires_days=None,
                                   httponly=True)
        content_type = self.request.headers.get("Content-Type")
        if content_type and "application/json" in content_type:
            self.json_args = tornado.escape.json_decode(self.request.body)

    # def on_finish(self):
    #     self.db.close()

    # @property
    # def db(self):
    #     return self.application.db

    @property
    def redis(self):
        return self.application.redis

    def get_current_user(self):
        try:
            session = self.session.load()
        except InvalidSessionException:
            user = None
        else:
            user_id = session.get('user_id')
            user = User.get(User.id == user_id)
        return user

    def get_template_namespace(self):
        namespace = super(BaseHandler, self).get_template_namespace()

        kwargs = dict(
            SITE_NAME=self.application.settings.get('SITE_NAME'),
            REGISTER_ENABLE=self.application.settings.get('REGISTER_ENABLE'),)

        namespace.update(kwargs)
        return namespace

    def response_json(self, json_str):
        self.set_header('Content-Type', 'application/json')
        if not isinstance(json_str, str):
            json_str = json.dumps(json_str)
        self.finish(json_str)

    # def get_current_user(self):
    #     username = self.get_secure_cookie('user')
    #     if username:
    #         username = tornado.escape.json_decode(username)
    #         user = self.db.get('select * from user where username=%s',
    #                            (username,))
    # return None if not user else user
    #         if not user:
    #             self.clear_cookie('user')
    #             return None
    #         else:
    #             return user
    #     else:
    #         return None

    # def render(self, template_name, **template_vars):
    #     html = self.render_string(template_name, **template_vars)
    #     self.finish(html)

    # def render_string(self, template_name, **template_vars):
    #     template = self.jinja2.get_template(template_name)
    #     return template.render(**template_vars)

# coding:utf8

import json
# import tornado.escape
from base import BaseHandler
import lib.auth as auth
from lib.utils import PeeweeSerializer

from model.user import User


class LoginHandler(BaseHandler):

    def get(self):
        self.render('login.html')

    def post(self):
        remember_me = self.get_argument('remember_me', False)
        user = auth.login(self,
                          username=self.json_args.get('username'),
                          password=self.json_args.get('password'))
        if user:
            # self.session.user = dict(id=user.id, username=user.username)
            self.session.user_id = user.id
            expires_days = 30 if remember_me else None
            self.session.save(expires_days=expires_days)
            # self.redirect(
            #     self.get_argument('next', None) or self.reverse_url('index'))
            url = self.get_argument('next', None) or self.reverse_url('index')
            rtn = dict(success=True, url=url,
                       user=user.serialize_object())
        else:
            message = u'Invalid Username or Password'
            rtn = dict(success=False, message=message)
        self.write(rtn)


class LogoutHandler(BaseHandler):

    def get(self):
        self.session.clear()
        self.redirect(self.reverse_url('index'))


class ChangePasswordHandler(BaseHandler):

    def get(self):
        session = self.session.load()
        # user = session.get('user')
        # print user
        _user = self.storm.get(User, User.id == 1)
        print _user

    def post(self):
        session = self.session.load()
        print session.get('user')


class SignUpHandler(BaseHandler):

    def get(self):
        pass

    def post(self):
        username = self.get_argument('username', None)
        password = self.get_argument('password', None)
        email = self.get_argument('email', None)
        print username, password, email
        self.db.insert()

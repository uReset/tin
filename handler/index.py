# coding:utf8

import tornado.web

from base import BaseHandler
from model.user import User


class IndexHandler(BaseHandler):

    @tornado.web.removeslash
    # @tornado.web.authenticated
    def get(self):
        # if not self.current_user:
        #     return self.render('index.html')
        # else:
        #     return self.render('home.html')
        return self.render("home.html")


class UserHomeHandler(BaseHandler):

    def get(self, username):
        target = self.storm.find(User, User.username == username).one()
        if not target:
            raise tornado.web.HTTPError(404)
        return self.render('home.html')

# coding:utf8

import json
from peewee import PeeweeException
import tornado.web

from lib.utils import PeeweeSerializer
from base import BaseHandler
from model.light import (Light,
                         UserLight)


class LightHandler(BaseHandler):

    # @tornado.web.authenticated
    def get(self):
        if self.current_user:
            light = Light.select().join(UserLight).where(
                UserLight.user == self.current_user)
        else:
            light = Light.select().order_by(Light.create_time.desc()).limit(10)

        serial = PeeweeSerializer()
        data = []
        for l in light:
            data.append(serial.serialize_object(l, depth=2))
        self.response_json(data)

    def post(self):
        title = self.json_args.get('title', None)
        content = self.json_args.get('content', None)

        try:
            light = Light.create(title=title, content=content,
                                 owner=self.current_user)
        except PeeweeException, e:
            raise e
            resp = 'oops'
        else:
            resp = light.serialize_object()
        return self.response_json(resp)


class FollowLightHandler(BaseHandler):

    def get(self):
        rtn = dict(success=False, message=None)
        light_id = self.get_argument(u'light', None)
        try:
            light = Light.get(Light.id == light_id)
        except Light.DoesNotExist:
            light = None

        if light:
            try:
                exist = UserLight.select().where(
                    (UserLight.user == self.current_user) &
                    (UserLight.light == light)
                ).get()
            except UserLight.DoesNotExist:
                exist = False

            if not exist:
                UserLight.create(user=self.current_user, light=light)
                rtn['success'] = True
                rtn['message'] = u'You followed success'
            else:
                rtn['message'] = u'Already followed'
        else:
            rtn['message'] = u'No object found'
        return self.response_json(json.dumps(rtn))

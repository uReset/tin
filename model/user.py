# coding:utf8

import datetime
from peewee import (PrimaryKeyField,
                    CharField,
                    DateTimeField)

from lib.hail import BaseModel


class User(BaseModel):
    id = PrimaryKeyField()
    username = CharField(unique=True,)
    nickname = CharField()
    password = CharField()
    email = CharField(unique=True)
    avatar = CharField(null=True)
    join_date = DateTimeField(default=datetime.datetime.now)

    display_fields = ("id", "username", "nickname", "email", "avatar")

    class Serializer:
        exclude_fields = ('password',)
        display_fields = ('id', 'username', 'nickname', 'email', 'avatar')

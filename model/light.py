# coding:utf8

import datetime
from peewee import (PrimaryKeyField,
                    CharField,
                    IntegerField,
                    ForeignKeyField,
                    TextField,
                    DateTimeField)

from lib.hail import BaseModel

from user import User


class Light(BaseModel):
    id = PrimaryKeyField()
    owner = ForeignKeyField(User, related_name='light_user_id_fk')
    title = CharField()
    content = TextField()
    star = IntegerField(default=0,)
    create_time = DateTimeField(default=datetime.datetime.now)

    display_fields = ("id", "title", "content", "star", "create_time", "owner")

    @classmethod
    def create(cls, **query):
        with cls._meta.database.transaction():
            light = super(Light, cls).create(**query)
            UserLight.create(user=query['owner'], light=light)
        return light


class UserLight(BaseModel):
    user = ForeignKeyField(User, related_name='user_light_user_id_fk')
    light = ForeignKeyField(Light, related_name='user_light_light_id_fk')

    class Meta:
        db_table = 'user_light'

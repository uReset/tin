# coding:utf8

import json
import pickle


# class Session(dict):

#     def __init__(self, session_manager, request_handler):
#         self.session_manager = session_manager
#         self.request_handler = request_handler

#     def save(self, expires_days=None):
#         self.session_manager.set(self.request_handler, self, expires_days)


# class SessionManager(object):

#     def __init__(self, redis, expires_days=None):
#         self.redis = redis
#         self.expires_days = expires_days

#     def set(self, request_handler, session, expires_days=None):
#         request_handler.set_secure_cookie('sessionid', session.session_id,
#                                   expires_days=expires_days)
#         session_data = json.dumps(session)
#         return self.redis.set(session.id, session_data)

#     def get(self, session_id):
#         data = self.redis.get(session_id)
#         if not data:
#             raise InvalidSessionException()
#         session = json.loads(data)
#         return session


class Session(object):

    __slots__ = ('request_handler', 'store', '_data')

    def __init__(self, request_handler, store):
        self.request_handler = request_handler
        self.store = store
        self._data = dict()

    def save(self, expires_days=None):
        rst = self.store.set(request_handler=self.request_handler,
                             session_data=self._data,
                             expires_days=expires_days)
        return rst

    def load(self):
        self._data = self.store.get(self.request_handler)
        return self._data

    def clear(self):
        rst = self.store.clear(self.request_handler)
        self.request_handler.clear_cookie('sessionid')
        return rst

    def get(self, name, default=None):
        try:
            return self.__getitem__(name)
        except KeyError:
            return default

    def __setattr__(self, name, value):
        if name in self.__slots__:
            object.__setattr__(self, name, value)
        else:
            self._data.__setitem__(name, value)

    def __getattr__(self, name):
        return self._data.__getitem__(name)

    def __setitem__(self, name, value):
        self._data.__setitem__(name, value)

    def __getitem__(self, name):
        return self._data.__getitem__(name)

    def __delattr__(self, name):
        self._data.__delattr__(name)


class RedisStore(object):

    def __init__(self, redis):
        self.redis = redis

    def set(self, request_handler, session_data, expires_days=None):
        # session_id = session_data.pop('session_id')
        # request_handler.set_secure_cookie('sessionid', session_id,
        #                                   expires_days=expires_days)
        session_id = request_handler.get_secure_cookie('sessionid')
        # session_data = json.dumps(session_data)
        session_data = pickle.dumps(session_data)
        self.redis.set(session_id, session_data)
        if expires_days:
            request_handler.set_secure_cookie(
                'sessionid', session_id, expires_days=expires_days,
                httponly=True)
            self.redis.expire(session_id, expires_days*24*3600)
        else:
            self.redis.expire(session_id, 1*24*3600)

    def get(self, request_handler):
        sessionid = request_handler.get_secure_cookie('sessionid')
        if not sessionid:
            raise InvalidSessionException()
        session_data = self.redis.get(sessionid)
        if not session_data:
            raise InvalidSessionException()
        # session = json.loads(session_data)
        # return session
        session = Session(request_handler, self.redis)
        # session._data = json.loads(session_data)
        session._data = pickle.loads(session_data)
        return session

    def clear(self, request_handler):
        sessionid = request_handler.get_secure_cookie('sessionid')
        return self.redis.delete(sessionid)


class InvalidSessionException(Exception):
    pass


if __name__ == '__main__':
    session = Session('a', 'request_handler')
    session['test'] = 'yes'
    print session
    print session.items()

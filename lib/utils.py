# coding:utf8


import json
import time
import datetime

from peewee import ForeignKeyField
# from storm.info import get_cls_info
# from storm.store import ResultSet


# def tin_json_encoder(ignore=()):

#     class CustomJSONEncoder(json.JSONEncoder):

#         def default(self, obj):
#             return encode_storm_object(obj, ignore)

#     return CustomJSONEncoder

    # if not hasattr(obj, "__storm_table__"):
    #     raise TypeError(repr(obj) + " is not JSON serializable")

    # result = {}
    # cls_info = get_cls_info(obj.__class__)
    # for name in cls_info.attributes.iterkeys():
    #     value = getattr(obj, name)
    #     if isinstance(value, datetime.datetime):
    #         value = time.mktime(value.timetuple())
    #     result[name] = value
    # return result


# def encode_storm_object(obj, ignore=(), depth=1):
#     if hasattr(obj, '__storm_table__'):
#         display_fields = getattr(obj, '__display_fields__', None)

#         result = {}
#         cls_info = get_cls_info(obj.__class__)
#         if display_fields:
#             fields = [field for field in cls_info.attributes.iterkeys() if
#                       field in display_fields]
#         else:
#             fields = [field for field in cls_info.attributes.iterkeys()]

#         for name in fields:
#             value = getattr(obj, name)
#             if isinstance(value, datetime.datetime):
#                 value = time.mktime(value.timetuple())
#             result[name] = value

#         relation_fields = getattr(obj, '__relation_fields__', None)
#         depth = depth and getattr(obj, '__relation_depth__', 0)
#         if relation_fields and depth:
#             for local, remote in relation_fields:
#                 result[local] = encode_storm_object(
#                     getattr(obj, remote), depth=0)
#         return result
#     elif isinstance(obj, ResultSet):
#         return list(obj)
#     else:
#         raise TypeError(repr(obj) + " is not JSON serializable")


def get_dictionary_from_model(model, fields=None, exclude=None, depth=1):
    # data = {}
    # model_class = type(model)

    # fields = fields or model._meta.get_field_names()
    # exclude = exclude or ()
    # curr_fields = model._meta.get_field_names()

    # _fields = set(fields) & set(curr_fields) - set(exclude)
    # # print fields,curr_fields, set(fields) & set(curr_fields)

    # for field_name in _fields:
    #     field_obj = model_class._meta.fields[field_name]
    #     field_data = model._data.get(field_name)
    #     # if isinstance(field_obj, ForeignKeyField) and field_data and \
    #     #         field_obj.rel_model in fields:
    #     if isinstance(field_obj, ForeignKeyField) and field_data:
    #         rel_obj = getattr(model, field_name)
    #         if depth > 1:
    #             data[field_name] = get_dictionary_from_model(
    #                 rel_obj, fields, exclude, depth=depth - 1)
    #         else:
    #             # data[field_name] = rel_obj.get_id
    #             data[field_name] = field_data
    #     else:
    #         data[field_name] = field_data

    # return data
    model_class = type(model)
    data = {}

    fields = fields or {}
    exclude = exclude or {}
    curr_exclude = exclude.get(model_class, [])
    curr_fields = fields.get(model_class, model._meta.get_field_names())

    for field_name in curr_fields:
        if field_name in curr_exclude:
            continue
        field_obj = model_class._meta.fields[field_name]
        field_data = model._data.get(field_name)
        if isinstance(field_obj, ForeignKeyField) and field_data and field_obj.rel_model in fields:
            rel_obj = getattr(model, field_name)
            data[field_name] = get_dictionary_from_model(rel_obj, fields, exclude)
        else:
            data[field_name] = field_data
    return data


class PeeweeSerializer(object):

    def convert_value(self, value):
        if isinstance(value, datetime.datetime) or \
                isinstance(value, datetime.date) or \
                isinstance(value, datetime.time):
            return time.mktime(value.timetuple())
        else:
            return value

    def clean_data(self, data):
        for key, value in data.items():
            if isinstance(value, dict):
                self.clean_data(value)
            elif isinstance(value, (list, tuple)):
                data[key] = map(self.clean_data, value)
            else:
                data[key] = self.convert_value(value)
        return data

    def serialize_object(self, obj, fields=None, exclude=None, depth=1):
        data = get_dictionary_from_model(obj, fields, exclude, depth)
        return self.clean_data(data)

peewee_serializer = PeeweeSerializer()

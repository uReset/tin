# coding:utf8

import uuid
import hashlib
import base64


# from model.user import User


def encrypt_password(password):
    salt = uuid.uuid4().bytes
    pw_salt = bytes(password) + salt
    hash_pw = hashlib.sha1(pw_salt).digest()
    secret = base64.b64encode(hash_pw + b'|' + salt)
    return secret


def decrypt_password(secret):
    pw_salt, salt = base64.b64decode(secret).split(b'|')
    return pw_salt, salt


def check_password(secret, input_password):
    pw_salt, salt = base64.b64decode(secret).split(b'|')
    _input = hashlib.sha1(bytes(input_password) + salt).digest()
    return _input == pw_salt


def regist_user(request_handler):
    pass


def login(request_handler, username=None, password=None):
    username = username or request_handler.get_argument('username', None)
    password = password or request_handler.get_argument('password', None)

    if not username and password:
        return None

    # _sql = ("select * from user where username=%(username)s"
    #         " or email=%(email)s")
    # user = request_handler.db.get(_sql, username=username, email=username)

    user = User.get(
        (User.username == username) | (User.email == username))

    if not user:
        return None

    # secret = user.pop('password')
    secret = user.password
    _pass = check_password(secret, password)
    if _pass:
        return user
    else:
        return None


def change_password(request_handler, current_password, new_password):
    pass


if __name__ == '__main__':
    print encrypt_password('c4ca4238a0b923820dcc509a6f75849b')

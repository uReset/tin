# coding:utf8

import json
from peewee import (with_metaclass,
                    Proxy,
                    Model, BaseModel)
from utils import PeeweeSerializer

db_proxy = Proxy()


class BaseModel(Model):

    class Meta:
        database = db_proxy

    class Serializer:
        exclude_fields = None
        display_fields = None

    def serialize_object(self, fields=None, exclude=None, depth=1):
        ps = PeeweeSerializer()
        return ps.serialize_object(self, fields, exclude, depth)

    def to_json(self, fields=None, exclude=None, depth=1):
        return json.dumps(self.serialize_object(self, fields, exclude, depth))


class SerializerOptions(object):
    def __init__(self, cls, **kwargs):
        self.model_class = cls
        self.fields = kwargs.get('fields', ())
        self.exclude = kwargs.get('exclude', ())
        self.depth = kwargs.get('depth', 0)


class SerializerMetaClass(type):

    def __new__(cls, name, bases, attrs):
        if not bases:
            return super(SerializerMetaClass, cls).__new__(cls, name, bases,
                                                           attrs)
        meta_option = {}
        meta = attrs.pop('Meta', None)
        if meta:
            for k, v in meta.__dict__.items():
                if not k.startswith('_'):
                    meta_option[k] = v
        new_obj = super(SerializerMetaClass, cls).__new__(cls, name, bases,
                                                          attrs)
        new_obj._meta = SerializerOptions(new_obj, **meta_option)
        return new_obj


class Serializer(with_metaclass(SerializerMetaClass)):

    _option_class = SerializerOptions

    def __init__(self, *args, **kwargs):
        pass

    def get_fields(self):
        exclude = None


class ModelSerializer(Serializer):
    pass


class HyperlinkeSerializer(ModelSerializer):
    pass


# class PeeweeSerializer(object):
#     def convert_value(self, value):
#         if isinstance(value, datetime.datetime) or \
#                 isinstance(value, datetime.date) or \
#                 isinstance(value, datetime.time):
#             return time.mktime(value.timetuple())
#         else:
#             return value
#     def clean_data(self, data):
#         for key, value in data.items():
#             if isinstance(value, dict):
#                 self.clean_data(value)
#             elif isinstance(value, (list, tuple)):
#                 data[key] = map(self.clean_data, value)
#             else:
#                 data[key] = self.convert_value(value)
#         return data
#     def serialize_object(self, obj, fields=None, exclude=None, depth=1):
#         data = get_dictionary_from_model(obj, fields, exclude, depth)
#         return self.clean_data(data)
if __name__ == '__main__':
    serializer = Serializer()
    print dir(serializer)

#!/bin/bash/env python
# coding:utf8

import os.path
import logging
import ConfigParser

import redis
from peewee import MySQLDatabase

# import torndb
import tornado.web
import tornado.ioloop
import tornado.options
import tornado.httpserver
from tornado.options import (parse_command_line,
                             options,
                             define,)

# from jinja2 import Environment, FileSystemLoader
import controler
from lib.session import RedisStore
from lib.hail import db_proxy


cfg = ConfigParser.ConfigParser()
cfg.read('config_sample.conf')


define('port', default=cfg.getint('APPLICATION', 'port'), type=int,
       help='listening port')
define('address', default=cfg.get('APPLICATION', 'address'), type=str,
       help='Server bind ip address')


class Application(tornado.web.Application):

    def __init__(self, *args, **kwargs):
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), 'templates'),
            static_path=os.path.join(os.path.dirname(__file__), 'static'),
            cookie_secret=cfg.get('SITE', 'cookie_secret'),
            login_url='/login',
            # jinja2=Environment(
            #     loader=FileSystemLoader(
            #         os.path.join(os.path.dirname(__file__), 'templates'))),
            gzip=True,
            debug=cfg.getboolean('APPLICATION', 'debug'),
            REGISTER_ENABLE=cfg.getboolean('SITE', 'register_enable'),
            SITE_NAME=cfg.get('SITE', 'site_name'),
        )
        handlers = controler.handlers
        # self.db = torndb.Connection(host=cfg.get('DATABASE', 'host'),
        #                             database=cfg.get('DATABASE', 'database'),
        #                             user=cfg.get('DATABASE', 'user'),
        #                             password=cfg.get('DATABASE', 'password'),
        #                             )
        self.redis = redis.StrictRedis(host=cfg.get('REDIS', 'host'),
                                       port=cfg.get('REDIS', 'port'),
                                       db=cfg.get('REDIS', 'db'))
        self.redis_store = RedisStore(self.redis)
        tornado.web.Application.__init__(self, handlers, **settings)
        # super(Application, self).__init__(handlers, **settings)


def main():
    parse_command_line()
    mysql_db = MySQLDatabase(database=cfg.get('DATABASE', 'database'),
                             host=cfg.get('DATABASE', 'host'),
                             port=cfg.getint('DATABASE', 'port'),
                             user=cfg.get('DATABASE', 'user'),
                             password=cfg.get('DATABASE', 'password'),)
    app = Application()
    db_proxy.initialize(mysql_db)

    # app.listen(port=options.port, address=options.address)
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    logging.warn('Server start at %s:%r' % (options.address, options.port))
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()

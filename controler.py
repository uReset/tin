# coding:utf8

from tornado.web import url

import handler.index
import handler.user
import handler.light


handlers = (
    url(r'/', handler.index.IndexHandler, name='index'),
    url(r'/login', handler.user.LoginHandler, name='login'),
    url(r'/logout', handler.user.LogoutHandler, name='logout'),
    url(r'/change-password', handler.user.ChangePasswordHandler,
        name='change_password'),
    url(r'/api/light', handler.light.LightHandler, name='api_light'),
    url(r'/api/light/follow', handler.light.FollowLightHandler,
        name='api_follow_light'),
    url(r'/home/(\w+)', handler.index.UserHomeHandler, name='user_home'),
)
